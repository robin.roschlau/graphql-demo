package com.github.roschlau.graphqldemo.ktor_graphql

import com.coxautodev.graphql.tools.GraphQLResolver
import com.coxautodev.graphql.tools.SchemaParser
import graphql.schema.GraphQLScalarType
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.util.pipeline.PipelineContext
import org.reflections.Reflections
import org.reflections.scanners.ResourcesScanner
import org.slf4j.LoggerFactory
import java.util.regex.Pattern

const val DEFAULT_GQL_ROUTE = "/graphql"
const val DEFAULT_SCHEMA_DIRECTORY = "graphql"


fun Route.graphQL(
    route: String = DEFAULT_GQL_ROUTE,
    schemaDirectory: String = DEFAULT_SCHEMA_DIRECTORY,
    provideContext: (ApplicationCall) -> Any,
    scalars: Array<out GraphQLScalarType> = arrayOf(),
    resolvers: Array<GraphQLResolver<*>> = arrayOf()
) {
    val files = Reflections(schemaDirectory, ResourcesScanner()).getResources(Pattern.compile(""".*\.graphqls"""))
    graphQL(route,
        GraphQLEndpoint(
            buildSchema(files, scalars, *resolvers),
            provideContext
        )
    )
}

private fun buildSchema(schemaFiles: Collection<String>, scalars: Array<out GraphQLScalarType>, vararg resolvers: GraphQLResolver<*>) =
    SchemaParser.newParser()
        .let { schemaFiles.fold(it) { builder, file -> builder.file(file) } }
        .resolvers(*resolvers)
        .scalars(*scalars)
        .build()
        .makeExecutableSchema()

/**
 * Sets up an endpoint that responds to GraphQL queries using the passed instance of [GraphQLEndpoint] at the given
 * [route]. The endpoint responds to GET requests as well as POST requests following the convention specified in
 * [the GraphQL documentation](http://graphql.org/learn/serving-over-http/#http-methods-headers-and-body)
 */
fun Route.graphQL(
    route: String = DEFAULT_GQL_ROUTE,
    endpoint: GraphQLEndpoint
) {
    get(route) { doGraphQLCall(endpoint) }
    post(route) { doGraphQLCall(endpoint) }
}

suspend fun PipelineContext<Unit, ApplicationCall>.doGraphQLCall(endpoint: GraphQLEndpoint) {
    val result = try {
        endpoint.processCall(call)
    } catch (e: InvalidRequestFormatException) {
        LoggerFactory.getLogger("GraphQL").warn("GraphQL request could not be parsed.", e)
        return call.respond(HttpStatusCode.BadRequest, e.message ?: "GraphQL request could not be parsed.")
    }
    return call.respondText(result, ContentType.Application.Json)
}
