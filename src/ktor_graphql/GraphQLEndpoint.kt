package com.github.roschlau.graphqldemo.ktor_graphql

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import graphql.ExecutionInput
import graphql.ExecutionResult
import graphql.GraphQL
import graphql.execution.UnknownOperationException
import graphql.schema.GraphQLSchema
import io.ktor.application.ApplicationCall
import io.ktor.request.contentType
import io.ktor.request.receive
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

private val jackson = ObjectMapper()

class GraphQLEndpoint(
    schema: GraphQLSchema,
    private val provideContext: (ApplicationCall) -> Any
) {

    private val graphQl = GraphQL.newGraphQL(schema).build()

    /**
     * Attempts to extract a GraphQL query from the passed [call] object, executes it, an returns the result as JSON.
     * Getting the query, operation name and variables from the [call] follows the best practices laid out in the
     * [GraphQL specification](http://graphql.org/learn/serving-over-http/), as does the format of the returned String.
     */
    @Throws(InvalidRequestFormatException::class)
    suspend fun processCall(call: ApplicationCall): String {
        val request = try {
            GraphQLRequest.from(call)
        } catch (e: JsonProcessingException) {
            throw InvalidRequestFormatException("GraphQL request could not be parsed.", e)
        }
        try {
            return graphQl
                .execute(request.toExecutionInput(provideContext(call)))
                .toSpecificationJson()
        } catch (e: UnknownOperationException) {
            throw InvalidRequestFormatException(e.message ?: UnknownOperationException::class.simpleName)
        }
    }

    private fun GraphQLRequest.toExecutionInput(context: Any): ExecutionInput {
        return ExecutionInput.Builder()
            .query(query)
            .operationName(operationName)
            .context(context)
            .root("Root")
            .variables(variables)
            .build()
    }

    private fun ExecutionResult.toSpecificationJson() = jackson.writeValueAsString(this.toSpecification())

}

class InvalidRequestFormatException(message: String?, cause: Throwable? = null) : Exception(message, cause)

private class GraphQLRequest(
    val query: String = "",
    val operationName: String? = null,
    variables: Map<String, Any>? = null
) {

    val variables = variables ?: mapOf()

    companion object {

        /**
         * Extracts a GraphQLRequest from the [call] according to the
         * [GraphQL Specification](http://graphql.org/learn/serving-over-http/#http-methods-headers-and-body).
         */
        @Throws(IOException::class, JsonParseException::class, JsonMappingException::class)
        suspend fun from(call: ApplicationCall): GraphQLRequest = withContext(Dispatchers.IO) {
            val contentType = call.request.contentType().contentSubtype
            when {
                call.parameters["query"] != null -> requestFromParameters(call)
                contentType == "graphql" -> GraphQLRequest(call.receive())
                else -> call.receive()
            }
        }

        /**
         * Builds a [GraphQLRequest] instance from the `query`, `operationName` and `variables` parameters of the [call]
         * object. These can either be GET- or POST-parameters.
         */
        @Throws(IOException::class, JsonParseException::class, JsonMappingException::class)
        private fun requestFromParameters(call: ApplicationCall): GraphQLRequest {
            val query = call.parameters["query"]!!
            val operationName = call.parameters["operationName"]
            val variables = call.parameters["variables"]?.let { paramValue ->
                jackson.readValue<Map<String, Any>>(paramValue)
            } ?: mapOf()
            return GraphQLRequest(query, operationName, variables)
        }
    }
}
