package com.github.roschlau.graphqldemo.ktor_graphql

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import java.io.BufferedReader

/**
 * Sets up a GET endpoint at the current route that serves a GraphiQL interface
 */
fun Route.graphiQL() {
    get("/") {
        val graphiql = resourceText("/graphiql.html")
        call.respondText(graphiql, ContentType.Text.Html)
    }
}

inline fun <reified T> T.resourceText(file: String): String =
    T::class.java.getResourceAsStream(file)
        .bufferedReader()
        .use(BufferedReader::readText)
