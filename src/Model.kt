package com.github.roschlau.graphqldemo

object Database {

    private val tasks = mutableListOf<Task>()
    private val users = mutableListOf<User>()

    fun getTasks() =
        tasks.toList()

    fun getTask(id: Id<Task>) =
        tasks.find { it.id == id }

    fun newTask(text: String) =
        Task(text = text)
            .also { tasks.add(it) }

    fun getUsers() =
        users.toList()

    fun getUser(id: Id<User>) =
        users.find { it.id == id }

    fun newUser(email: String, name: String) =
        User(email = email, name = name)
            .also { users.add(it) }

    fun deleteTask(id: Id<Task>): List<Task> {
        tasks.removeIf { it.id == id }
        return getTasks()
    }

}

class Task(
    val id: Id<Task> = Id.random(),
    var text: String,
    var done: Boolean = false,
    var assignedToUser: Id<User>? = null
) {
    fun id() = id.value
}

class User(
    val id: Id<User> = Id.random(),
    val email: String,
    val name: String
) {
    fun id() = id.value
}
