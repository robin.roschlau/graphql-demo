package com.github.roschlau.graphqldemo

import com.fasterxml.jackson.databind.SerializationFeature
import com.github.roschlau.graphqldemo.ktor_graphql.graphQL
import com.github.roschlau.graphqldemo.ktor_graphql.graphiQL
import com.github.roschlau.graphqldemo.resolvers.Mutation
import com.github.roschlau.graphqldemo.resolvers.Query
import com.github.roschlau.graphqldemo.resolvers.TaskResolver
import com.github.roschlau.graphqldemo.resolvers.UserResolver
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.features.*
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import org.slf4j.event.Level

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    setupServer()

    routing {
        get("/health") {
            call.respond(HttpStatusCode.OK, "Server is healthy")
        }
        graphiQL()
        graphQL(
            provideContext = ::Context,
            resolvers = arrayOf(
                Query(),
                Mutation(),
                TaskResolver(),
                UserResolver()
            )
        )
    }
}

class Context(val call: ApplicationCall)



fun Application.setupServer() {
    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(AutoHeadResponse)

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost()
    }

    install(Authentication) {
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }
}
