package com.github.roschlau.graphqldemo

import java.util.UUID

/**
 * Superclass for all classes that wrap a single value for typesafety purposes.
 */
abstract class WrapperType<T : Any>(val value: T) {
    override fun toString() = value.toString()
    override fun equals(other: Any?) = other is WrapperType<*> && value == other.value
    override fun hashCode() = value.hashCode()
}

/**
 * Wrap entity ids in an instance of this class parameterized with a type identifying the entity the id belongs to,
 * to reduce the likeliness of accidentally cross-assigning ids to the wrong types.
 *
 * Example:
 *
 * ```kotlin
 *     data class Thing1(val id: Id<Thing1>)
 *     data class Thing2(val id: Id<Thing2>)
 *
 *     fun getMoreThings(thingId: Id<Thing1>)
 *
 *     val thing: Thing2
 *     getMoreThings(thing.id) // Won't compile!
 * ```
 */
@Suppress("unused") // Only used for compile time validation
class Id<out T>(id: String) : WrapperType<String>(id), Comparable<Id<*>> {

    override fun compareTo(other: Id<*>) = value.compareTo(other.value)
    companion object {
        fun <T> random(length: Int = DEFAULT_ID_LENGTH) = Id<T>(randomIdString(length))
    }
}

private val chars = ('0'..'9') + ('A'..'Z') + ('a'..'z')
private const val DEFAULT_ID_LENGTH = 11
fun randomIdString(length: Int = DEFAULT_ID_LENGTH) = String(CharArray(length) { chars.random() })
