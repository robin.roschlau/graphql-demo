@file:Suppress("unused")

package com.github.roschlau.graphqldemo.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver
import com.github.roschlau.graphqldemo.Id
import com.github.roschlau.graphqldemo.Task
import com.github.roschlau.graphqldemo.Database
import com.github.roschlau.graphqldemo.User

class Query : GraphQLQueryResolver {

    fun tasks(done: Boolean?, assignedUsers: List<Id<User>>?) =
        Database.getTasks()
            .filter { done == null || it.done == done }
            .filter { assignedUsers == null || it.assignedToUser in assignedUsers }

    fun users() =
        Database.getUsers()

}

class Mutation : GraphQLMutationResolver {

    fun taskCreate(text: String) =
        Database.newTask(text)

    fun taskSetDone(id: Id<Task>, done: Boolean) =
        Database.getTask(id)?.apply { this.done = done }

    fun taskEdit(id: Id<Task>, setText: String?, assignUser: Id<User>?) =
        Database.getTask(id)?.apply {
            if (setText != null) text = setText
            if (assignUser != null) assignedToUser = assignUser
        }

    fun userCreate(email: String, name: String) =
        Database.newUser(email = email, name = name)
}

class TaskResolver : GraphQLResolver<Task> {

    fun assignedToUser(task: Task) =
        task.assignedToUser?.let(Database::getUser)
}

class UserResolver : GraphQLResolver<User> {

    fun assignedTasks(user: User) =
        Database.getTasks().filter { it.assignedToUser == user.id }
}
